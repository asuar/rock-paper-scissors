# rock-paper-scissors

Created with Javascript as part of The Odin Project [Curriculum](https://www.theodinproject.com/courses/web-development-101/). 

# Final Thoughts 

Initial work on this project allowed me to review some basic Javascript methods and language structure.

Additional work on this project allowed me to review using DOM manipulation to make page content dynamic and Javascript EventListeners.
