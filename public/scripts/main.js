  let playerScore = 0;
  let computerScore = 0;
  const MAX_ROUNDS = 5;
  let gameOver = false;

  function random(a) {
    return Math.floor(Math.random() * a) + 1;
  }

  function capitalize(string) {
    let firstLetter = string.substr(0, 1);
    return firstLetter.toUpperCase() + string.substr(1);
  }

  function computerPlay() {
    let computerChoice = random(3);

    switch (computerChoice) {
      case 1:
        return "paper";
        break;
      case 2:
        return "rock";
        break;
      case 3:
        return "scissors";
        break;
    }
  }

  function getRoundResult(playerSelection, computerSelection) {
    const TIE = -1;
    const WIN = 1;
    const LOSE = 0;
    
    if (playerSelection == computerSelection) 
      return TIE;

    if (playerSelection == "paper") {
      if (computerSelection == "rock"){
        incrementPlayerScore();
        return WIN;
      }else{
        incrementComputerScore();
        return LOSE;
      }
    } else if (playerSelection == "rock") {
      if (computerSelection == "scissors"){
        incrementPlayerScore();
        return WIN;
      }else{
        incrementComputerScore();
        return LOSE;
      }
    } else {
      if (computerSelection == "paper"){
        incrementPlayerScore();
        return WIN;
      }else{
        incrementComputerScore();
        return LOSE;
      }
    }
  }

  function chooseMessage(roundResult, playerSelection, computerSelection){
    if(roundResult == -1 ){
      return `Tie! ${capitalize(playerSelection)} vs ${capitalize(computerSelection)}`;
    }else if (roundResult == 0){
      return `You Lose! ${capitalize(computerSelection)} beats ${capitalize(playerSelection)}`;
    }else{
      return `You Win! ${capitalize(playerSelection)} beats ${capitalize(computerSelection)}`;
    }
  }

  function createButtons(){
    const gameButtons = document.querySelectorAll('.gameButton');
    gameButtons.forEach((button) => button.addEventListener('click', (e) => {
      playRound(e.target.id);
    }));

    document.querySelector('#reset').addEventListener('click', (e)=>{
      resetGame();
    });
  }

  function playRound(playerChoice){

    if(!gameOver){
      let computerChoice = computerPlay();
      let roundResult = getRoundResult(playerChoice,computerChoice);
      updateScore();
      updateResults(chooseMessage(roundResult,playerChoice,computerChoice));
      if(determineGameOver()){
        updateResults(declareWinner());
        gameOver = true;
      }
    }
  }

  function incrementPlayerScore(){
    playerScore++;
  }

  function incrementComputerScore(){
    computerScore++;
  }

  function displayScore(){
    const container = document.querySelector('#score');
    const content = document.createElement('p');
    content.textContent = `Your Score: ${playerScore}           Computer Score: ${computerScore}`;

    container.appendChild(content);
  }

  function updateScore(){
    const text = document.querySelector('p');
    text.textContent = `Your Score: ${playerScore}           Computer Score: ${computerScore}`;
  }

  function initResults(){
    const container = document.querySelector('#results');
    const content = document.createElement('p');
    content.setAttribute('id', 'gameResults');

    container.appendChild(content);
  }

  function updateResults(message){
    const text = document.querySelector('#gameResults');
    text.textContent = message;
  }

  function initGame(){
    createButtons();
    displayScore();
    initResults();
  }

  function determineGameOver(){
    if(playerScore >= MAX_ROUNDS || computerScore >= MAX_ROUNDS){
      return true;
    }else{
      return false;
    }
  }

  function declareWinner(){
    if(playerScore == computerScore)
      return "Game was a draw!";
    else if (playerScore < computerScore)
      return "You lose the game! :(";
    else
      return "You win the game! :)";
  }

  function resetGame(){
    playerScore = 0;
    computerScore = 0;
    gameOver = false;
    updateScore();
    updateResults("");
  }

initGame();